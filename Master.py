from apscheduler.schedulers.blocking import BlockingScheduler
import Speedtest
import logging
# import Source


def run_test():
    print("Master is invoking Speedtest...")
    results = Speedtest.run()

scheduler = BlockingScheduler()
logging.getLogger('apscheduler.executors.default').propagate = False
scheduler.add_job(run_test, 'interval', minutes=10)
scheduler.start()
