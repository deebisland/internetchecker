// this file is no longer used
send_email_to = "kedy6yar@nottingham.edu.my\n";
email_title = "internet problem";
email_body = "Internet is down";
check_counter = 0;

function new_email() {
    if (document.querySelectorAll('[title="Write a new message (N)"]')[0] === undefined) {
    }
    else {
        document.querySelectorAll('[title="Write a new message (N)"]')[0].click();
        clearInterval(new_email_interval);
    }
}
new_email_interval = setInterval(new_email, 2000);

function set_to() {
    if (document.querySelectorAll('[aria-label="To"]')[0] === undefined) {
    }
    else {
        document.querySelectorAll('[aria-label="To"]')[0].value = send_email_to;
        ++check_counter;
        clearInterval(set_to_interval);
    }
}
set_to_interval = setInterval(set_to, 2000);

function set_subject() {
    if (document.querySelectorAll('[placeholder="Add a subject"]')[0] === undefined) {
    }
    else {
        document.querySelectorAll('[placeholder="Add a subject"]')[0].value = email_title;
        ++check_counter;
        clearInterval(set_subject_interval);
    }
}
set_subject_interval = setInterval(set_subject, 2000);

function set_body() {
    if (document.querySelectorAll('[style="margin-top:0;margin-bottom:0"]')[0] === undefined) {
    }
    else {
        document.querySelectorAll('[style="margin-top:0;margin-bottom:0"]')[0].innerHTML = email_body;
        ++check_counter;
        clearInterval(set_body_interval);
    }
}
set_body_interval = setInterval(set_body, 2000);

function send_email() {
    if (check_counter === 3) {
        document.querySelectorAll('[title="Send"]')[0].click();
        clearInterval(send_email_interval);
    }
}
send_email_interval = setInterval(send_email, 2000);
