from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from = "kedy6yar@nottingham.edu.my"
to = "kedy6yar@nottingham.edu.my"
subject = "internet problem"
body = "internet very bad"

with open("password.txt", "r") as f:
    password = f.read()
driver = webdriver.Chrome()  # Optional argument, if not specified will search path.
actions = ActionChains(driver)
driver.get("https://outlook.office.com")
with open("index.js") as file:
    script = file.read()
input_email_address = driver.find_element_by_id("i0116")
input_email_address.send_keys(from)
script_next = "btnNext = document.getElementById('idSIButton9'); btnNext.click();"
driver.execute_script(script_next)
input_password = driver.find_element_by_name("Password")
input_password.send_keys(password)
input_sign_in = driver.find_element_by_id("submitButton")
input_sign_in.submit()
input_yes = driver.find_element_by_id("idSIButton9")
input_yes.submit()
script_new_email = "btnNewEmail = document.getElementById('lnkHdrnewmsg'); btnNewEmail.click();"
driver.execute_script(script_new_email)
input_to = driver.find_element_by_id("txtto")
input_to.send_keys(to)
input_subject = driver.find_element_by_id("txtsbj")
input_subject.send_keys(subject)
input_body = driver.find_element_by_name("txtbdy")
input_body.send_keys(body)
input_send_email = "btnNewEmail = document.getElementById('lnkHdrsend'); btnNewEmail.click();"
driver.execute_script(input_send_email)
driver.quit()

# the server id for speedtest-cli is 14565
