import os
import logging

SPEEDTEST_CMD = "\"C:/Program Files (x86)/Python36-32\Scripts/speedtest-cli.exe\""
LOG_FILE = "speedtest.log"


def setup_logging():
    logging.basicConfig(
        filename=LOG_FILE,
        level=logging.INFO,
        format="%(asctime)s %(message)s",
        datefmt="%Y-%m-%d %H:%M",
    )


def get_speedtest_results():
    ping = download = upload = None
    with os.popen(SPEEDTEST_CMD + " --simple") as speedtest_output:
        for line in speedtest_output:
            label, value, unit = line.split()
            if "Ping" in label:
                ping = float(value)
            elif "Download" in label:
                download = float(value)
            elif "Upload" in label:
                upload = float(value)

    if all((ping, download, upload)):  # if all 3 values were parsed
        return ping, download, upload
    else:
        raise ValueError("TEST FAILED")


def run():
    print("Speedtest is running...")
    setup_logging()
    try:
        results = get_speedtest_results()
        ping, download, upload = results
    except ValueError as err:
        logging.info(err)
    else:
        logging.info("%5.1f %5.1f %5.1f", ping, download, upload)
        return results
